﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using dotNetProject.Models;
using dotNetProject.Helpers;

namespace dotNetProject.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var response = Authentication.Login(model.Username, model.Password);

            switch (response.status)
            {
                case Authentication.Status.Success:
                    HttpContext.GetOwinContext().Authentication.SignIn(
                        new AuthenticationProperties { IsPersistent = false }, response.identity
                    );

                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                case Authentication.Status.UserDoesntExist:
                    ModelState.AddModelError("", "Username doesnt exist in database.");
                    break;
                case Authentication.Status.InvalidPassword:
                    ModelState.AddModelError("", "Invalid password.");
                    break;
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    break;
            }

            return View(model);
        }
        
        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var response = Authentication.Register(model.Username, model.Password);

            switch (response.status)
            {
                case Authentication.Status.Success:
                    HttpContext.GetOwinContext().Authentication.SignIn(
                        new AuthenticationProperties { IsPersistent = false }, response.identity
                    );

                    return RedirectToAction("Index", "Home");
                case Authentication.Status.UserAlreadyExist:
                    ModelState.AddModelError("", "Given username already exist in database.");
                    break;
                case Authentication.Status.Failure:
                    ModelState.AddModelError("", "Cannot create account.");
                    break;
            }

            return View(model);
        }
        
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Index", "Home");
        }
    }
}