﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using dotNetProject.Helpers;
using dotNetProject.Models;

namespace dotNetProject.Controllers
{
    [Authorize]
    public class PostsController : Controller
    {
        // GET: Posts
        [AllowAnonymous]
        public ActionResult Index()
        {
            var model = Database.GetAllPosts();

            return View(model);
        }

        // POST: Posts/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(PostViewModel model)
        {
            var post = new Post()
            {
                Content = model.Content,
                Author = User.Identity.Name
            };

            Database.Insert(post);

            return RedirectToAction("Index", "Posts");
        }

        // GET: Posts/Delete
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            var post = Database.GetPost(id);

            if (post != null)
            {
                Database.Delete(post);
            }

            return RedirectToAction("Index", "Posts");
        }
    }
}