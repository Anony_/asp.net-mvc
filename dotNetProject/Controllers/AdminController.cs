﻿using dotNetProject.Helpers;
using System.Web.Mvc;

namespace dotNetProject.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            var model = Database.GetAllAccounts();

            return View(model);
        }

        // POST: Admin/ChangeRole
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeRole(int id)
        {
            var account = Database.GetAccount(id);

            account.Role = account.Role == "User" ? "Admin" : "User";
            Database.Update(account);

            return RedirectToAction("Index", "Admin");
        }
    }
}