﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using dotNetProject.Models;

namespace dotNetProject.Helpers
{
    public class Database
    {
        public static Account GetAccount(string username)
        {
            using (var session = CreateSessionFactory().OpenSession())
            {
                var account = session
                    .QueryOver<Account>()
                    .Where(x => x.Username == username)
                    .List()
                    .FirstOrDefault();

                return account;
            }
        }

        public static Account GetAccount(int id)
        {
            using (var session = CreateSessionFactory().OpenSession())
            {
                var account = session
                    .QueryOver<Account>()
                    .Where(x => x.Id == id)
                    .List()
                    .FirstOrDefault();

                return account;
            }
        }

        public static IList<Account> GetAllAccounts()
        {
            using (var session = CreateSessionFactory().OpenSession())
            {
                var accounts = session
                    .QueryOver<Account>()
                    .List()
                    .OrderBy(x => x.Id);

                return accounts.ToList();
            }
        }

        public static bool CreateAccount(Account account)
        {
            using (var session = CreateSessionFactory().OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(account);
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
            }

            return true;
        }

        public static IList<Post> GetAllPosts()
        {
            using (var session = CreateSessionFactory().OpenSession())
            {
                var posts = session
                    .QueryOver<Post>()
                    .List()
                    .OrderBy(x => x.Id);

                return posts.ToList();
            }
        }

        public static Post GetPost(int id)
        {
            using (var session = CreateSessionFactory().OpenSession())
            {
                var post = session
                    .QueryOver<Post>()
                    .Where(x => x.Id == id)
                    .List()
                    .FirstOrDefault();

                return post;
            }
        }

        public static bool Update<T>(T entity)
        {
            using (var session = CreateSessionFactory().OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Update(entity);
                        transaction.Commit();
                    }
                    catch (StaleObjectStateException)
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
            }

            return true;
        }

        public static bool Delete<T>(T entity)
        {
            using (var session = CreateSessionFactory().OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(entity);
                        transaction.Commit();
                    }
                    catch (StaleObjectStateException)
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
            }

            return true;
        }

        public static bool Insert<T>(T entity)
        {
            using (var session = CreateSessionFactory().OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Save(entity);
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
            }

            return true;
        }

        private static ISessionFactory CreateSessionFactory()
        {
            ISessionFactory isessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2005.ConnectionString(ConfigurationManager.AppSettings["MSSQLconnection"]))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Database>())
                .BuildSessionFactory();

            return isessionFactory;
        }
    }
}