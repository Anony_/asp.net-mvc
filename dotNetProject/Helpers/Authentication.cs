﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using dotNetProject.Models;
using dotNetProject.Helpers;
using System.Security.Claims;
using Microsoft.AspNet.Identity;

namespace dotNetProject.Helpers
{
    public static class Authentication
    {
        public enum Status
        {
            Success,
            Failure,
            InvalidPassword,
            UserDoesntExist,
            UserAlreadyExist
        }

        public struct Response
        {
            public ClaimsIdentity identity;
            public Status status;
        }

        public static Response Login(string username, string password)
        {
            var account = Database.GetAccount(username);
            var response = new Response();

            if (account == null)
            {
                response.status = Status.UserDoesntExist;
                response.identity = null;
            }
            else if (!BCrypt.Net.BCrypt.Verify(password, account.Password))
            {
                response.status = Status.InvalidPassword;
                response.identity = null;
            }
            else
            {
                response.status = Status.Success;
                response.identity = GetIdentity(account);
            }

            return response;
        }

        public static Response Register(string username, string password)
        {
            if (Database.GetAccount(username) != null)
            {
                return new Response()
                {
                    identity = null,
                    status = Status.UserAlreadyExist
                };
            }

            var account = new Account()
            {
                Username = username,
                Password = BCrypt.Net.BCrypt.HashPassword(password, 10),
                Role = "User"
            };

            var result = Database.CreateAccount(account);
            var response = new Response()
            {
                identity = GetIdentity(account),
                status = result ? Status.Success : Status.Failure
            };

            return response;
        }

        public static Response ChangePassword(string username, string oldPassword, string newPassword)
        {
            var account = Database.GetAccount(username);

            if (account == null)
            {
                return new Response()
                {
                    status = Status.UserDoesntExist
                };
            }
            
            if (!BCrypt.Net.BCrypt.Verify(oldPassword, account.Password))
            {
                return new Response()
                {
                    status = Status.InvalidPassword
                };
            }

            account.Password = BCrypt.Net.BCrypt.HashPassword(newPassword, 10);

            var result = Database.Update(account);

            var response = new Response()
            {
                identity = result ? Authentication.GetIdentity(account) : null,
                status = result ? Status.Success : Status.Failure
            };

            return response;
        }

        private static ClaimsIdentity GetIdentity(Account account)
        {
            return new ClaimsIdentity(
                new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, account.Username),
                    new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string"),
                    new Claim(ClaimTypes.Name, account.Username),
                    new Claim(ClaimTypes.Role, account.Role)
                },
                DefaultAuthenticationTypes.ApplicationCookie
            );
        }
    }
}