﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(dotNetProject.Startup))]
namespace dotNetProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
