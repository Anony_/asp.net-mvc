﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotNetProject.Models
{
    public class Account
    {
        public virtual int Id { get; protected set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual string Role { get; set; }
    }
}