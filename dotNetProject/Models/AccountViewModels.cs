﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace dotNetProject.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [CustomValidation(typeof(RegisterViewModel), "AtLeastOneDigit")]
        [CustomValidation(typeof(RegisterViewModel), "AtLeastOneBigLetter")]
        [CustomValidation(typeof(RegisterViewModel), "AtLeastOneSpecialCharacter")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public static ValidationResult AtLeastOneBigLetter(string password, ValidationContext validationContext)
        {
            if (Regex.Match(password, @".*?[A-Z].*").Success)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult("Password must have at least one uppercase letter.");
        }

        public static ValidationResult AtLeastOneSpecialCharacter(string password, ValidationContext validationContext)
        {
            if (Regex.Match(password, @".*?[^\w\s].*").Success)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult("Password must have at least one special character.");
        }

        public static ValidationResult AtLeastOneDigit(string password, ValidationContext validationContext)
        {
            if (Regex.Match(password, @".*?\d.*").Success)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult("Password must have at least one digit.");
        }
    }
}
