﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotNetProject.Models
{
    public class Post
    {
        public virtual int Id { get; protected set; }
        public virtual string Author { get; set; }
        public virtual string Content { get; set; }
    }
}