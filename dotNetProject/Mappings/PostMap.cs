﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using dotNetProject.Models;
using FluentNHibernate.Mapping;

namespace dotNetProject.Mappings
{
    public class PostMap : ClassMap<Post>
    {
        public PostMap()
        {
            Id(x => x.Id);
            Map(x => x.Author);
            Map(x => x.Content);
        }
    }
}