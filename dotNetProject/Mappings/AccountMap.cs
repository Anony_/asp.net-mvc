﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using dotNetProject.Models;
using FluentNHibernate.Mapping;

namespace dotNetProject.Mappings
{
    public class AccountMap : ClassMap<Account>
    {
        public AccountMap()
        {
            Id(x => x.Id);
            Map(x => x.Username);
            Map(x => x.Password);
            Map(x => x.Role);
        }
    }
}